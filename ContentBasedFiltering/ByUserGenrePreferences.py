from Database.DBContext import DBContext
from Database.DBOperations import DBOperations
from CosineSimilarity.Cosine_Similarity import Cosine_Similarity

class ByUserGenrePreferences:

   def __init__(self):
      self._dbContext = DBContext()
      self.sqlOperations = DBOperations()
      self.cosine_similarity = Cosine_Similarity()

   def main(self):
      """
      Main Function
      """
      try:
         userPref = self.sqlOperations.getUserPreferences()
         movieList = self.sqlOperations.getMoviesList(True, "2.50")
         for user in userPref:
            self.calculateRecommendations_BasedOnUserGenrePreferences(user, movieList)
      except Exception as ex:
         print(ex)

   '''
   Function generates recommendations based on user preferences
   '''
   def calculateRecommendations_BasedOnUserGenrePreferences(self, user, movieList):
      cosineSim = []
      userPref = []
      if(user.preferences):
         userPref = user.preferences.split(',')
      
      for movie in movieList:
         genre=[]
         userPrefVector=[]
         genreVector=[]
         if(movie.genre):
            genre = movie.genre.split(',')
         
         union = list(set(userPref+genre))
         for cat in union:
            if cat in userPref: userPrefVector.append(1)
            else: userPrefVector.append(0)
            if cat in genre: genreVector.append(1)
            else: genreVector.append(0)

         similarity = self.cosine_similarity.cosine_similarity(userPrefVector, genreVector)
         if(similarity > 0.6):
            cosineSim.append({'id':movie.id, 'similarity': similarity})

      cosineSim.sort(reverse=True, key=self.cosine_similarity.sortBySimilarity)
      self.saveRecommendations(userid=user.id, movieIds=','.join([str(o['id']) for o in cosineSim[:10]]))

   '''
   Saves Recommendations
   '''
   def saveRecommendations(self, userid, movieIds):
      self._dbContext.insertUserSuggestionsIntoTable(userid=userid, moviesuggestions_bypref=movieIds, moviesuggestions_byself=None, moviesuggestions_byusersimilarity=None)


