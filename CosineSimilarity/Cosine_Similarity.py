import numpy as np
from Database.DBContext import DBContext
from Database.DBOperations import DBOperations

class Cosine_Similarity:

    def __init__(self):
        self._dbContext = DBContext()
        self.sqlOperations = DBOperations()

    '''
    Calculates Cosine Similarity
    '''
    def cosine_similarity(self, x, y):
        # Ensure length of x and y are the same
        if len(x) != len(y) :
            return None
        # Compute the dot product between x and y
        dot_product = np.dot(x, y)
        # Compute the L2 norms (magnitudes) of x and y
        magnitude_x = np.sqrt(np.sum(self.powerof2(x))) 
        magnitude_y = np.sqrt(np.sum(self.powerof2(y)))
        # Compute the multiplication
        multiply = magnitude_x * magnitude_y

        if(multiply==0):
            return 0

        return dot_product / multiply

    '''
    Function calculates power of 2
    '''
    def powerof2(self,my_list):
        return [ x**2 for x in my_list ]

    '''
    Function sorts by similarity
    '''
    def sortBySimilarity(self, e):
        return e['similarity']