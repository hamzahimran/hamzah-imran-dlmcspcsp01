# Python code to demonstrate working of unittest
import unittest
import importlib


class TestMethods(unittest.TestCase):
	
	def setUp(self):
		pass

	def test_AssertSqlAlchemyExists(self):
		self.assertTrue(importlib.find_loader('sqlalchemy'))

	def test_AssertNumpyExist(self):		
		self.assertTrue(importlib.find_loader('numpy'))

if __name__ == '__main__':
	unittest.main()

