# HamzahImran_Project



## Steps to clone the project
Prerequisites
1. You should have git installed in your machine
2. You should be logged in to your git account 

1. Login to github from browser
2. Go to project dashboard
3. Copy the clone link
4. Open Command Prompt
5. Type "Git clone <GIT PROJECT URL>" and hit enter
6. Project should be cloned in your local

## Steps to make changes and push to branch
1. Check out to a new branch from develop branch -- git checkout -b <branch name>
2. Make your changes
3. Add your changes to git -- git add <file name>
4. Commit the changes to new branch -- git commit -m <message>
5. Push the changes to your branch -- git push --set-upstream origin <branch name>
6. Create a merge request and add a reviewer
7. Merge your changes post review


## Files description

1. Main.py
    It is the root file. Its the spine of the whole project.
    Once run it will take care of the complete flow i.e. from getting the data from JSON to database
    to getting to different suggestions based on user behavior

2. DBContext.py
    This file is responsible for all the database operations.
    On Initialization , It establishes a database connection to the database using  a connection string
    It consists of multiple functions, for each, please read the description in the docstrings
    Libraries used: sqlalchemy, sqlalchemy_utils, cryptography

3. DBOperations.py
    This file performs various queries and fetches business specific data from the database.

4. Setup_Data.py
    This file creates a fresh instance of the database
    Libraries used - sys, json

5. Cosine_Similarity.py
    This file consists of logic specific to Cosine Similarity.
    Library Used - numpy

6. BySameUserSeenAndLikedMovieSimilarity.py
    This file is responsible to calculate and get suggestions to the user based on the movies he/she has seen and has given a high rating. We also do a of the genre of the movies liked by the user and suggest the same
    Example - If User has liked Movie A and Movie B having Genre [A,B,C] and [B,C,E] respectively
    We suggest more movies to the user with genre [A,B], [A,B,E] etc. So to enable better personalization.

7. CompareUsersMovieGenreSimilarity.py
    This file is responsible to calculate and get suggestions to the user by finding the similarity between the users. The tool believes that users with similar likes have similar tastes.
    Example - If User A has liked Movie A and Movie B having Genre [A,B,C] and [B,C,E] respectively and 
    another User B has liked Movie A and Movie C having Genre [A,B,C] and [C,F,G] respectively
    We suggest movie C to User A. So to enable better personalization.

8. ByUserGenrePreferences.py
    This file is responsible to calculate and get suggestions to the user by finding the similarity between the preferences given by the user in terms of genre and the movies.
    Example - If User has preference of Genre [A,B,C]
    We suggest movies to the user with genre [A,B], [A,B,E] etc. So to enable better personalization.

9. UnitTests.py
    This file performs Unit tests.

## Few Notes
1. If you want to execute this project please make sure you have a SQL database installed.
   Update the connection string accordingly in the DBContext.py

2. Please make sure you use correct paths for all the .json files.

3. The startup file is Main.py

4. Please make sure you have below libraries installed.
   These libraries are must for the running of this project
    * sqlalchemy
    * cryptography
    * sqlalchemy_utils
    * numpy

5. This project once run will automatically pick the data provided in the json and will
   generate recommendations for the users.

6. Please follow the below steps to see the generated recommendations.
   * Go to SQL Workbench
   * use <your db name>
   * select * from user_suggestions;
   * moviesuggestions_bypref column shows recommendations per user based on preference
   * moviesuggestions_bypref column shows recommendations per user based on user similarity
   * moviesuggestions_byself column shows recommendations per user based on the 
     movies liked by the user himself/herself
