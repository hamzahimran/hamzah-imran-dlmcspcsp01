import json;
from cryptography import sys
from Database.DBContext import DBContext

class Setup:

   def __init__(self):
      self._dbContext = DBContext()

   def main(self):
      """
      Function is used to do a fresh seeding in of the data.
      """
      try:
         self.DropAllTables()
         self.CreateAllTables()
         self.InsertDataIntoTables()
      except Exception as ex:
         print(ex)
         sys.exit()

   def CreateAllTables(self):
      """
      Function creates new tables
      """
      self._dbContext.createTables("movie_list", "id INT(5), moviename VARCHAR(100), genre VARCHAR(50), productrating DECIMAL(4,2), reviewcount INT(6)")
      self._dbContext.createTables("user_preferences", "id INT(5), city VARCHAR(20), preferences VARCHAR(100)")
      self._dbContext.createTables("user_suggestions", "userid INT(5), moviesuggestions_bypref VARCHAR(100), moviesuggestions_byself VARCHAR(100), moviesuggestions_byusersimilarity VARCHAR(100), moviesuggestions_consodilated VARCHAR(1000)")
      self._dbContext.createTables("user_rating", "id INT(5), movieid INT(5), productrating DECIMAL(4,2)")

   def InsertDataIntoTables(self):

      """
      Function inserts data into tables
      """
      movieListJson = open("SeedData/movie_list.json")
      userPrefJson = open("SeedData/user_preferences.json")
      userRatingJson = open("SeedData/user_rating.json")
      movieList = json.load(movieListJson)
      userPref = json.load(userPrefJson)
      userRating = json.load(userRatingJson)

      for obj in movieList:
         self._dbContext.insertMovieListIntoTable(obj['id'],obj['moviename'],','.join(obj['genre']),obj['productrating'],obj['reviewcount'])

      for obj in userPref:
         self._dbContext.insertUserPrefIntoTable(obj['id'],obj['city'],','.join(obj['preferences']))

      for obj in userRating:
         self._dbContext.insertUserRatingIntoTable(obj['id'],obj['movieid'],obj['productrating'])

   def DropAllTables(self):
      """
      Function drops all tables 
      """
      self._dbContext.DropTableIfExist("movie_list")
      self._dbContext.DropTableIfExist("user_preferences")
      self._dbContext.DropTableIfExist("user_rating")
      self._dbContext.DropTableIfExist("user_suggestions")
      