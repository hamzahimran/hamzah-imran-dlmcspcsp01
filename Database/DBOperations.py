from Database.DBContext import DBContext

class DBOperations:

    def __init__(self):
        self._dbContext = DBContext()

    '''
    Function gets the userid and preferences of the user in terms of genre
    '''
    def getUserPreferences(self):
        try:
            userPref = self._dbContext.PerformGetQuery("SELECT id, preferences FROM user_preferences")
            return userPref
        except Exception as ex:
            print("Unable to get User Preferences.")
            print(ex)

    '''
    Gets all the movies from the movie database. This function can also handle filtering by product rating
    '''
    def getMoviesList(self, filterbyrating, rating):
        try:
            query = "SELECT id, genre, productrating FROM movie_list"
            if filterbyrating:
                query+=" WHERE productrating >= "+rating

            movieList = self._dbContext.PerformGetQuery(query)
            return movieList
        except Exception as ex:
            print("Unable to movie list.")
            print(ex)

    '''
    Gets the movies seen by the user. Movies can also be filtered by the user rating
    '''
    def getMoviesSeenByAllUsers(self, productrating):
        try:
            movieList = self._dbContext.PerformGetQuery("SELECT ur.id as userid, ur.movieid, ml.genre "+
                "FROM user_rating as ur "+
                "JOIN movie_list as ml ON ur.movieid=ml.id "+
                "WHERE ur.productrating>="+productrating+" ORDER BY ur.id;")
            return movieList
        except Exception as ex:
            print("Unable to get movies seen by all users filtered by product rating.")
            print(ex)

    '''
    Gets all the movies seen and rated > 3.0 by the user
    '''
    def getAllHighlyRatedMoviesAndGenrePerUser(self):
        try:
            userLikedMovies = self.getMoviesSeenByAllUsers(productrating="3.0")
            cumulativeUserGenreList=[]
            genreLikedByUser=[]
            moviesSeenPerUser=[]
            userId=None
            for movie in userLikedMovies:
                if(userId == None):
                    userId=movie.userid
                
                if(userId != movie.userid):
                    cumulativeUserGenreList.append({'userid': userId, 'moviesSeen':list(set(moviesSeenPerUser)),'genre': list(set(genreLikedByUser))})
                    userId=movie.userid
                    genreLikedByUser=[]
                    moviesSeenPerUser=[]

                genreLikedByUser+=movie.genre.split(',')
                moviesSeenPerUser.append(movie.movieid)
            cumulativeUserGenreList.append({'userid': userId, 'moviesSeen':list(set(moviesSeenPerUser)), 'genre': list(set(genreLikedByUser))})
            return cumulativeUserGenreList
        except Exception as ex:
            print(ex)