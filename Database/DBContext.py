import sqlalchemy as db
from cryptography import sys
import sqlalchemy_utils as sql_utils

class DBContext:
    
    def __init__(self):
        """
        Constructor establishes the database connection
        """
        try:
            self.engine = db.create_engine("mysql+pymysql://root:hamzah@localhost/pypy")
            if not sql_utils.database_exists(self.engine.url):
                sql_utils.create_database(self.engine.url)
            self.connection  = self.engine.connect()
            self.meta_data = db.MetaData(bind=self.engine)
            db.MetaData.reflect(self.meta_data)
        except Exception as ex:
            print("Error in establishing connection to database.")
            print(ex)
            sys.exit()

    def createTables(self, tableName, keys):
        """
        Function creates table in database
        Input:
            tableName - Name of table
            keys - Columns with definition
        """
        try:
            self.engine.execute("CREATE TABLE IF NOT EXISTS "+tableName+ " ("+keys+")")
            self.meta_data.create_all(self.engine)
            db.MetaData.reflect(self.meta_data)
        except Exception as ex:
            print("Error in table creation.")
            print(ex)
            sys.exit()

    '''
    Inserts movies to database table
    '''
    def insertMovieListIntoTable(self,id,moviename,genre,productrating,reviewcount):
        try:
            datasets_train = self.meta_data.tables["movie_list"]
            datasets_train.insert().values(id=id,moviename=moviename,genre=genre,productrating=productrating,reviewcount=reviewcount).execute()
        except Exception as ex:
            print("Error in inserting movie data into table")
            print(ex)
            sys.exit()

    '''
    Inserts user preferences to the database table 
    '''
    def insertUserPrefIntoTable(self,id,city,preferences):
        try:
            datasets_train = self.meta_data.tables["user_preferences"]
            datasets_train.insert().values(id=id,city=city,preferences=preferences).execute()
        except Exception as ex:
            print("Error in inserting user preferences.")
            print(ex)
            sys.exit()

    '''
    Gets all rows from the specified table
    '''
    def selectAllRowsFromTable(self, tableName):
        datasets_train = self.meta_data.tables[tableName]
        result = datasets_train.select().execute()
        return result

    '''
    Inserts all the recommendations calculated by the system into the database table
    '''
    def insertUserSuggestionsIntoTable(self,userid,moviesuggestions_bypref, moviesuggestions_byself, moviesuggestions_byusersimilarity):
        try:
            datasets_train = self.meta_data.tables["user_suggestions"]
            query = datasets_train.select().where(datasets_train.columns.userid == userid).execute()
            if(query.rowcount == 0):
                if(moviesuggestions_bypref):
                    datasets_train.insert().values(userid=userid,moviesuggestions_bypref=moviesuggestions_bypref).execute()
                elif(moviesuggestions_byself):
                    datasets_train.insert().values(userid=userid,moviesuggestions_byself=moviesuggestions_byself).execute()
                else:
                    datasets_train.insert().values(userid=userid,moviesuggestions_byusersimilarity=moviesuggestions_byusersimilarity).execute()
            else:
                if(moviesuggestions_bypref):
                    datasets_train.update().where(datasets_train.columns.userid == userid).values(moviesuggestions_bypref=moviesuggestions_bypref).execute()
                elif(moviesuggestions_byself):
                    datasets_train.update().where(datasets_train.columns.userid == userid).values(moviesuggestions_byself=moviesuggestions_byself).execute()
                else:
                    datasets_train.update().where(datasets_train.columns.userid == userid).values(moviesuggestions_byusersimilarity=moviesuggestions_byusersimilarity).execute()
        except Exception as ex:
            print("Error in inserting calculated recommended data for users into table.")
            print(ex)
            sys.exit()
    
    '''
    Inserts consodilated user recommendations into the database table
    '''
    def insertConsodilatedUserRecommendationsIntoTable(self, userid, moviesuggestions_consodilated):
        datasets_train = self.meta_data.tables["user_suggestions"]
        results = datasets_train.select().where(datasets_train.columns.userid == userid).execute()
        if(results.rowcount == 0):
            datasets_train.insert().values(userid=userid,moviesuggestions_consodilated=moviesuggestions_consodilated).execute()
        else:
            datasets_train.update().where(datasets_train.columns.userid == userid).values(moviesuggestions_consodilated=moviesuggestions_consodilated).execute()

    '''
    Inserts user rating per movie in the database table
    '''
    def insertUserRatingIntoTable(self,id,movieid,rating):
        try:
            datasets_train = self.meta_data.tables["user_rating"]
            insertQuery = datasets_train.insert().values(id=id,movieid=movieid,productrating=rating)
            self.connection.execute(insertQuery)
        except Exception as ex:
            print("Error in inserting user provided ratings into table.")
            print(ex)
            sys.exit()

    '''
    Function drops table from database
    Input:
        tableName
    '''
    def DropTableIfExist(self, tableName):

        if tableName in self.meta_data.tables:
           self.engine.execute("DROP TABLE "+tableName)
           
    '''
    Function performs Get query
    input - SQL query
    '''
    def PerformGetQuery(self, query):
        return self.connection.execute(query).fetchall()