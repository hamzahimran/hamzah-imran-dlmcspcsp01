from Database.DBContext import DBContext
from Database.DBOperations import DBOperations
from CosineSimilarity.Cosine_Similarity import Cosine_Similarity



class BySameUserSeenAndLikedMovieSimilarity:

    def __init__(self):
        self._dbContext = DBContext()
        self.dbOperations = DBOperations()
        self.cosineSimilarity = Cosine_Similarity()

    def main(self):
        """
        Main Function
        """
        genrePerUser=self.dbOperations.getAllHighlyRatedMoviesAndGenrePerUser()
        movieList = self.dbOperations.getMoviesList(True, "3.00")
        for row in genrePerUser:
            self.getAndSaveRecommendationsPerUserLikedGenre(row,movieList)

    '''
    Function generates recommendations based on the movies highly rated by the user 
    '''
    def getAndSaveRecommendationsPerUserLikedGenre(self, userData, movieList):
        cosineSim = []

        for movie in movieList:
            genre=[]
            likedMovieGenreVector=[]
            genreVector=[]
            if(movie.id in userData['moviesSeen']):
                continue
            if(movie.genre):
                genre = movie.genre.split(',')
            
            union = list(set(userData['genre']+genre))
            for cat in union:
                if cat in userData['genre']: likedMovieGenreVector.append(1)
                else: likedMovieGenreVector.append(0)
                if cat in genre: genreVector.append(1)
                else: genreVector.append(0)

            similarity = self.cosineSimilarity.cosine_similarity(likedMovieGenreVector, genreVector)
            if(similarity >= 0.6):
                cosineSim.append({'id':movie.id, 'similarity': similarity})

        cosineSim.sort(reverse=True, key=self.cosineSimilarity.sortBySimilarity)
        self.saveUserSuggestions(userData['userid'], ','.join([str(o['id']) for o in cosineSim[:10]]))

    '''
    Saves Recommendations
    '''
    def saveUserSuggestions(self, userid, movieIds):
        self._dbContext.insertUserSuggestionsIntoTable(userid=userid, moviesuggestions_bypref = None, moviesuggestions_byusersimilarity = None, moviesuggestions_byself=movieIds)
