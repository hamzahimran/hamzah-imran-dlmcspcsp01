import numpy as np
from Database.DBContext import DBContext
from Database.DBOperations import DBOperations
from CosineSimilarity.Cosine_Similarity import Cosine_Similarity

# This class gets suggestions based on movies liked by self
# Gets all the similar movies depending upon the genres liked by the user
class CompareUsersMovieGenreSimilarity:

    def __init__(self):
        self._dbContext = DBContext()
        self.dbOperations = DBOperations()
        self.cosineSimilarity = Cosine_Similarity()

    def main(self):
        """
        Main Function
        """
        userData=self.dbOperations.getAllHighlyRatedMoviesAndGenrePerUser()
        recommendationsPerUser=self.getRecommendationsFromSimilarUsers(userData)

        for row in recommendationsPerUser:
            self.saveUserSuggestions(row['userid'],','.join([str(o) for o in row['suggestedMoviesFiltered']]))

    '''
    Function generates recommendations from similar kind of likings
    '''
    def getRecommendationsFromSimilarUsers(self, userData):
        similarUserPair=[]
        for i in range(len(userData)):
            cosineSimPerUser = []
            for j in range(len(userData)):
                if(i==j):
                    continue
                union = list(set(userData[i]['genre']+userData[j]['genre']))
                primUserGenre=[]
                secondUserGenre=[]
                for cat in union:
                    if cat in userData[i]['genre']: primUserGenre.append(1)
                    else: primUserGenre.append(0)
                    if cat in userData[j]['genre']: secondUserGenre.append(1)
                    else: secondUserGenre.append(0)
                similarity = self.cosineSimilarity.cosine_similarity(primUserGenre, secondUserGenre)
                if(similarity > 0.6):
                    cosineSimPerUser.append({'userid': userData[j]['userid'], 'moviesSeen':userData[j]['moviesSeen'],'similarity': similarity})

            cosineSimPerUser.sort(reverse=True, key=self.cosineSimilarity.sortBySimilarity)
            suggestedMovies = list(set(np.concatenate([o['moviesSeen'] for o in cosineSimPerUser[:3]])))[:12] if len([o['moviesSeen'] for o in cosineSimPerUser[:3]])> 0 else []
            suggestedMoviesFiltered = [index for index in suggestedMovies if index not in userData[i]['moviesSeen']]
            similarUserPair.append({'userid': userData[i]['userid'], 'suggestedMoviesFiltered': suggestedMoviesFiltered})
        return similarUserPair
    
    '''
    Saves Recommendations
    '''
    def saveUserSuggestions(self, userid, movieIds):
        self._dbContext.insertUserSuggestionsIntoTable(userid=userid, moviesuggestions_byusersimilarity=movieIds, moviesuggestions_byself=None, moviesuggestions_bypref=None)
