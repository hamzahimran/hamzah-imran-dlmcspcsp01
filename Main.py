from Database.DBContext import DBContext
from Database.Setup_Data import Setup
from ContentBasedFiltering.ByUserGenrePreferences import ByUserGenrePreferences
from CollaborationBasedFiltering.BySameUserSeenAndLikedMovieSimilarity import BySameUserSeenAndLikedMovieSimilarity
from CollaborationBasedFiltering.CompareUsersMovieGenreSimilarity import CompareUsersMovieGenreSimilarity

def main():
    """
    ****--**** Function Of Origin. ****--****
    ****--****     Start Here      ****--****
    """
    setup = Setup()
    setup.main()
    print("Setup done")
    byUserGenrePreferences = ByUserGenrePreferences()
    byUserGenrePreferences.main()
    print("Content Based Filtering done")
    bySameUserSeenAndLikedMovieSimilarity = BySameUserSeenAndLikedMovieSimilarity()
    bySameUserSeenAndLikedMovieSimilarity.main()
    print("Collaboration based filtering done")
    compareUsersMovieGenreSimilarity = CompareUsersMovieGenreSimilarity()
    compareUsersMovieGenreSimilarity.main()
    print('Suggestions based on similar user done')
    removeDuplicatesFromSuggestions()
    print("Consodilation Done")


'''
Creates a consodilated row having a union of all the recommendations. 
This consodilated list of suggestions is free from all duplicates.

'''
def removeDuplicatesFromSuggestions():
    db=DBContext()
    result = db.selectAllRowsFromTable("user_suggestions")
    for row in result:
        byPref = row['moviesuggestions_bypref'].split(',') if row['moviesuggestions_bypref'] != None else []
        bySelf = row['moviesuggestions_byself'].split(',') if row['moviesuggestions_byself'] != None else []
        byUserSimilarity = row['moviesuggestions_byusersimilarity'].split(',') if row['moviesuggestions_byusersimilarity'] != None else []
        filterUnionDuplicates = list(set(byPref+bySelf+byUserSimilarity))
        db.insertConsodilatedUserRecommendationsIntoTable(row['userid'], ','.join(filterUnionDuplicates))
    
if __name__ == '__main__':
    main()